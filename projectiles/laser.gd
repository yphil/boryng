extends Node2D

@onready var visible_on_screen_notifier_2d = $VisibleOnScreenNotifier2D

func _ready():
	visible_on_screen_notifier_2d.screen_exited.connect(queue_free)
