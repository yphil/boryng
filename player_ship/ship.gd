extends Node2D

@onready var left_muzzle = $LeftMuzzle
@onready var right_muzzle = $RightMuzzle
@onready var spawner_component = $SpawnerComponent
@onready var fire_rate_timer = $FireRateTimer

func _ready():
	fire_rate_timer.timeout.connect(fire_lasers)
	pass # Replace with function body.

func fire_lasers():
	spawner_component.spawn(left_muzzle.global_position)
	spawner_component.spawn(right_muzzle.global_position)
